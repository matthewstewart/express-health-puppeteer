class Timer {

  constructor(options) {
    this.options = options || {};
    this._running = false;
    this._hrtime = process.hrtime();
    this._diff = [0, 0];
  }

  get hrtime() {
    if (!this._hrtime) this._hrtime = process.hrtime();
    return this._hrtime;
  }

  get diff() {
    if (this._running) return process.hrtime(this._hrtime);
    return this._diff;
  }

  start() {
    this._running = true;
    this._hrtime = process.hrtime();
    return this.hrtime;
  }

  stop() {
    this._running = false;
    this._diff = process.hrtime(this.hrtime);
    return this.diff;
  }

  parse(format, suffix) {
    format = (format === undefined) ? this.options.format || "ms" : format;

    let val = null;

    switch (format) {
      case "ms":
        val = this.valueOf();
        val = (val * 1000).toFixed(6);
        break;
      default:
        val = this.valueOf();
    }
    return val;
  }

  result(format) {
    return Number(this.parse(format));
  }

  valueOf() { // seconds
    let diff = this.diff;
    return (diff[0] + (diff[1] * 1e-9)).toFixed(9);
  }
}

module.exports = Timer;