const listEndpoints = require('express-list-endpoints');
const Timer = require('./Timer');

module.exports = (options) => {
  let opts = Object.assign({}, options);

  if (options.rtHeader && !(typeof options.rtHeader === 'string' || options.rtHeader instanceof String)) {
    opts.rtHeader = options.rtHeader;
  } else {
    opts.rtHeader = false;
  }

  opts.format = options.rtFormat || "ms";

  return {
    listRoutes: (app) => listEndpoints(app),
    Timer: Timer,
    responseTimer: {
      start: (req, res, next) => {
        if (!req.rtime) req.rtime = new Timer(opts);
        req.rtime.start();
        next();
      },
      stop: (req, res, next) => {
        if (!req.rtime) req.rtime = new Timer(opts);
        req.rtime.stop();
        if (opts.rtHeader) res.set(opts.rtHeader, req.rtime.parse(opts.format));
        next();
      }
    }
  }
};