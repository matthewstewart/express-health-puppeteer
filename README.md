# Express Health Puppeteer
Utilities for:
- Monitoring the health of an Express applications
- Front End Testing using Puppeteer
- Server Testing

## Install
```
npm i express=health-puppeteer
```

## Health Examples

### Time Route Response Using Middleware
```javascript
const express = require('express');
const app = express();
const port = 3000;
const {responseTimer} = require('express-health-puppeteer')({
  rtHeader: 'X-Response-Time', // optional - set header key - is populated with response time
  rtFormat: 's' // optional - 's' = seconds (default), 'ms' = milliseconds
});

// start request timer before registering routes
// adds request time to req.rtime
app.use(responseTimer.start);


// use req.rtime.result(format)
// format can be:
// 's' - seconds
// 'ms' - milliseconds
// undefined - defaults to opts.rtFormat || seconds
app.get('/health', async (req, res) => {
  res.json({
    uptime: process.uptime(),
    responseTime: req.rtime.result()
  });
});

// after registering routes stop request timer
app.use(responseTimer.stop);

app.listen(port, () => console.log('listening on ' + port));
```

Return Example:
```javascript
{
  "uptime": 2.1306436,
  "responseTime": 0.0006627
}
```

